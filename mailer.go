package sender

import (
	"bytes"
	"fmt"
	"mime/quotedprintable"
	"net/smtp"
	"strings"
)

// EmailConfig - email config
type EmailConfig struct {
	AuthHost     string `json:"mail-auth-host,omitempty"`
	AuthUserName string `json:"mail-auth-username,omitempty"`
	AuthPassword string `json:"mail-auth-password,omitempty"`
	SMTPAddress  string `json:"mail-server-address,omitempty"`
}

// sendMail - send email
func (sender EmailConfig) sendMail(dest []string, Subject, bodyMessage string) {
	msg := fmt.Sprintf("From: %s\nTo: %s\nSubject: %s\n%s", sender.AuthUserName, strings.Join(dest, ","), Subject, bodyMessage)

	err := smtp.SendMail(sender.SMTPAddress,
		smtp.PlainAuth("", sender.AuthUserName, sender.AuthPassword, sender.AuthHost),
		sender.AuthUserName, dest, []byte(msg))
	if err != nil {
		fmt.Printf("smtp error: %s\n", err)
		return
	}
	fmt.Println("Mail sent successfully!")
}

// writeEmail - compose an email
func (sender EmailConfig) writeEmail(dest []string, contentType, subject, bodyMessage string) string {
	header := make(map[string]string)
	header["From"] = sender.AuthUserName
	receipient := strings.Join(dest, ",")

	header["To"] = receipient
	header["Subject"] = subject
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = fmt.Sprintf("%s; charset=\"utf-8\"", contentType)
	header["Content-Transfer-Encoding"] = "quoted-printable"
	header["Content-Disposition"] = "inline"

	message := ""
	for key, value := range header {
		message += fmt.Sprintf("%s: %s\r\n", key, value)
	}

	var encodedMessage bytes.Buffer
	finalMessage := quotedprintable.NewWriter(&encodedMessage)
	finalMessage.Write([]byte(bodyMessage))
	finalMessage.Close()

	message += "\r\n" + encodedMessage.String()

	return message
}

// SendHTMLEmail - send html encorded email
func (sender *EmailConfig) SendHTMLEmail(dest []string, subject, bodyMessage string) {
	body := sender.writeEmail(dest, "text/html", subject, bodyMessage)
	sender.sendMail(dest, subject, body)
}

// SendPlainEmail - send plain text email
func (sender *EmailConfig) SendPlainEmail(dest []string, subject, bodyMessage string) {
	body := sender.writeEmail(dest, "text/plain", subject, bodyMessage)
	sender.sendMail(dest, subject, body)
}
